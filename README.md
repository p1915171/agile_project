### PROJET DOBBLE - PTUT (S2-S4)

#### Informations générales de l'outil

Cet outil permet de générer un jeu de Dobble en PDF selon certains critères : 

- Type de symbole 
- Nombre de symboles par cartes
- Forme des cartes
- Couleur des cartes
- etc

#### Informations générales du projet

Ce projet est  développer majoritairement en JavaScript et est entièrement **open-source**, tout le monde est donc autorisé à le télécharger et à apporter des modifications selon votre usage.

Réalisé par : 

- Cassani Matthias : matthias.cassani@etu.univ-lyon1.fr
- Harzelli Youssef : youssef.harzelli@etu.univ-lyon1.fr
- Laroche Jordan : jordan.laroche@etu.univ-lyon1.fr
- Marques Paul : paul.marques@etu.univ-lyon1.fr

Tuteur : 

Watrigant Rémi 